// @source https://play.golang.org/p/_AfeTlQqOp
// inspired buut modified.
package ssh

import (
	"bufio"
	"bytes"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"runtime"
	"strings"

	"golang.org/x/crypto/ssh"
	"golang.org/x/term"
)

type Instance struct {
	Address string
	Client  *ssh.Client
	Session *ssh.Session
	Config  *ssh.ClientConfig
}

var StdoutBuf bytes.Buffer

var ErrLoginFailed1 = errors.New("failed to connect in original way")

// Connect gives an SSH instance that can be used to run commands on.
// ❗❕ Note that only 1 command will be able to be ran, then the instance
// will close again.
// If it returns the "failed to connect in original way",
// try to use LocalSshRunCommands() instead of Connect() and RunCommands()
func Connect(addr, user, pemPath string) (Instance, error) {
	errPassPhraseMissing := ssh.PassphraseMissingError{}

	pemBytes, err := ioutil.ReadFile(pemPath)
	if err != nil {
		return Instance{}, err
	}

	// Parse private key - assume it's not protected
	signer, err := ssh.ParsePrivateKey(pemBytes)
	if err.Error() == errPassPhraseMissing.Error() {
		// It's protected, so we ask for the passphrase
		var passphraseIn string

		fmt.Printf(`                    | 🔑 Enter passphrase for (%s) > `, pemPath)
		fd := int(os.Stdin.Fd())
		if term.IsTerminal(fd) {
			pwdBytes, err := term.ReadPassword(fd)
			fmt.Println("")
			if err != nil {
				return Instance{}, fmt.Errorf("failed to read passphrase : %v", err)
			}
			passphraseIn = string(pwdBytes)
		} else {
			scanner := bufio.NewScanner(os.Stdin)
			fmt.Print("\033[8m") // hide input
			if _, err := fmt.Scanln(&passphraseIn); err != nil {
				return Instance{}, err
			}
			fmt.Print("\033[28m") // show input
			if scanner.Err() != nil {
				fmt.Print("\033[28m") // show input
				return Instance{}, scanner.Err()
			}
		}
		////////////////

		// attempt to use the passphrase
		signer, err = ssh.ParsePrivateKeyWithPassphrase(pemBytes, []byte(passphraseIn))
		if err != nil {
			return Instance{}, err
		}
	}

	// Set the config for the SSH connection
	instance := Instance{}
	instance.Address = addr
	instance.Config = &ssh.ClientConfig{
		User:            user,
		Auth:            []ssh.AuthMethod{ssh.PublicKeys(signer)},
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
	}
	conn, err := ssh.Dial("tcp", instance.Address, instance.Config)
	if err != nil {

		return Instance{}, ErrLoginFailed1
	}
	session, err := conn.NewSession()
	if err != nil {
		return Instance{}, err
	}

	session.Stdout = &StdoutBuf

	instance.Client = conn
	instance.Session = session

	return instance, nil

}

func (instance *Instance) Close() {
	if err := instance.Session.Close(); err != nil {
		os.Exit(1)
	}
	if err := instance.Client.Close(); err != nil {
		os.Exit(1)
	}
}

// Run the command, closes the instance and replaces the old one by a new one.
func (instance *Instance) RunCommands(cmds ...string) (err error) {
	// maybe https://gist.github.com/Oppodelldog/a885759d209c6d8a5b7cd4b06bcb92e7 ?
	cmd := strings.Join(cmds, "; ")
	if err = instance.Session.Run(cmd); err != nil {
		return err
	}
	log.Printf(">%s", StdoutBuf.String())

	instance.Close()

	conn, err := ssh.Dial("tcp", instance.Address, instance.Config)
	if err != nil {
		return err
	}
	session, err := conn.NewSession()
	if err != nil {
		return err
	}
	session.Stdout = &StdoutBuf

	instance.Client = conn
	instance.Session = session

	return err
}

// LocalSshRunCommands runs the commands but using the local SSH.
// This can be useful if the standard Connect() and RunCommands() fail for
// some reasons.
func LocalSshRunCommands(addr, port, user string, cmds ...string) (err error) {
	if len(cmds) == 0 {
		return errors.New("no commands to run")
	}

	cmd := strings.Join(cmds, "; ")
	cmd = strings.Trim(cmd, "; ")

	// Set the command name to use depending on running operating system
	cmdName := "bash"
	if runtime.GOOS == "windows" {
		cmdName = "powershell"
	}

	// Run the SSH commands
	cmd = strings.ReplaceAll(cmd, string('"'), string([]byte{'\\', '"'}))
	// cmd = strings.ReplaceAll(cmd, string('\n'), string([]byte{'\\', 'n'}))
	acmd := exec.Command(cmdName, fmt.Sprintf(`ssh %s@%s -p %s '%s'`, user, addr, port, cmd))
	var out bytes.Buffer
	acmd.Stdout = &out

	// 2FA seems to be starting an StdinPipe
	// So lets close it once the command is done so it doesn't
	// stay on hold
	stdin, _ := acmd.StdinPipe()
	_ = acmd.Start()
	acmd.Wait()
	err = stdin.Close()
	if err != nil {
		log.Printf(`| 🟥  Still failing... Try to run manually the following by copy/pasting :
	
ssh %s@%s -p %s "%s"
		`, user, addr, port, cmd)
	}

	fmt.Println(acmd.Stdout)

	return err
}
