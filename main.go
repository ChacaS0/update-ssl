package main

import (
	"bufio"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"runtime"
	"strings"
	"time"

	"github.com/leaanthony/clir"
	"gitlab.com/ChacaS0/update-ssl/config"
	"gitlab.com/ChacaS0/update-ssl/ssh"

	scp "gitlab.com/ChacaS0/ssh_config_parser"
)

const version = "1.0.1"

const notes = ` 
Notes: 
	⚠ This requires you to have the SSH config file already set up.
	⚠ For now it works only with certs of the same types.
`

const description = `Bulk update your SSL certificates.`

// FgConfFile is the path to the configuration file
var FgConfFile string

// CLI is the CLI application
var CLI *clir.Cli

var conf = config.ConfStructure{}

const bannerLogo = `
       ▄▄                 ▄▄        App      : update-ssl 
      █████             ▄████▄      Created  : 2021-04-05
     ███████▄ ▄▄███▄▄ ▄███████▄     Version  : v` + version + `
    ██████████▀     ▀██████████     Project  : gitlab.com/ChacaS0/update-ssl
    ▐██████▀           ▀███████     
     ▀███▀               ▀███▀      Author   : Chacas0
       █▌                 ▐█        Profile  : gitlab.com/Chacas0
      ██                   ██       Website  : chacas0.gitlab.io
       ▀█▄▐█▄▄        ▄█▀ ██        
         █▄  ▀▀      ▀  ▄█▀         
          ▀█           ██           
           ▀█▄  ▀█▀▀ ▄█▀            
             ▀█▄▄█▄▄██              
                ▀▀▀▀                
`

func init() {
	CLI = clir.NewCli("update-ssl", description+"\n"+bannerLogo+notes, version)
	hdir, err := os.UserHomeDir()
	if err == nil {
		FgConfFile = hdir + string(os.PathSeparator) + ".config" + string(os.PathSeparator) + "update-ssl" + string(os.PathSeparator) + "config.yml"
		CLI.StringFlag("c", "Path to the configuration file (*.yml).", &FgConfFile)
	} else {
		log.Fatalln("| 🟥  Couldn't find a home directory with error :", err)
	}
}

func main() {

	CLI.Action(func() error {
		return action()
	})

	// Run the application
	err := CLI.Run()
	if err != nil {
		// We had an error
		log.Fatal(err)
	}

}

func action() error {
	fmt.Println(bannerLogo)

	sufix := time.Now().Unix()

	// Read & load config file
	if err := conf.LoadConfig(FgConfFile); err != nil {
		log.Fatalf("| 🟥  Couldn't read the configuration file and resulted witht he following error : %v\n", err)
	}
	log.Printf("| 🟦  You are about to deploy your certificates")
	// Ask/ Double check if this is the configuration file to use
	scanner := bufio.NewScanner(os.Stdin)
	fmt.Printf(`                    | 🔷   Use %s ? (Y/n) : `, FgConfFile)
	var runAnswer string
	// var runAnswer string
	if _, err := fmt.Scanln(&runAnswer); err != nil {
		return err
	}

	if scanner.Err() != nil {
		return scanner.Err()
	}
	runAnswer = strings.ToLower(runAnswer)
	if runAnswer != "" && runAnswer != "yes" && runAnswer != "y" {
		return errors.New("| 🟥  User exited")
	}

	// SSH mode to use
	var choiceHostSsh string
	fmt.Printf(`                    | 🔷   Use default SSH ? (Y/n) : `)
	if _, err := fmt.Scanln(&choiceHostSsh); err != nil {
		return err
	}
	choiceHostSsh = strings.ToLower(choiceHostSsh)
	if choiceHostSsh == "" || choiceHostSsh == "y" || choiceHostSsh == "yes" {
		choiceHostSsh = "y"
	} else if choiceHostSsh == "n" || choiceHostSsh == "no" {
		choiceHostSsh = "n"
	} else {
		return errors.New("| 🟥  Invalid choice. Exitting.")
	}

	log.Printf("| 🟦  KeepOldCerts is set to %t\n", conf.KeepOldCerts)

	// Get ssh configurations
	log.Println("| 🟨  Looking for SSH config at :", conf.SshConfFile)
	sshConfigs := getSshParams(conf.SshConfFile)
	log.Printf("| 🟩  Found %v hosts configured\n", len(sshConfigs))

	// Now act for each certs configuration
	for _, pair := range conf.Certs {
		certs, err := ioutil.ReadDir(pair.Local)
		if err != nil {
			log.Fatalf("| 🟥  Could not reach the directory : %s with the error : %v", pair.Local, err)
		}

		cmds := make([]string, len(certs)*3)

		for _, cert := range certs {
			// skip directories
			if cert.IsDir() {
				continue
			}

			// add it to our
			certData, err := getCertificateData(pair.Local + cert.Name())
			if err != nil {
				log.Fatalf("| 🟥  Could not read the certificate : %v\n", pair.Local+cert.Name())
			}

			log.Printf("| 🟩  Certificate %s found.", pair.Local+cert.Name())

			dest := strings.TrimRight(pair.Remote, "/")
			dest = fmt.Sprintf("%s/%d/", dest, sufix)

			// Create directory tree
			cmds = append(cmds, fmt.Sprintf("mkdir -vp %s", pair.Remote))
			// Copy only if source exists
			if conf.KeepOldCerts {
				cmds = append(cmds, fmt.Sprintf(`[ -f "%s" ] && mkdir -vp %s`, pair.Remote+cert.Name(), dest))
				cmds = append(cmds, fmt.Sprintf(`[ -f "%s" ] && cp -rv -b %s %s`, pair.Remote+cert.Name(), pair.Remote+cert.Name(), dest))
			}
			// Write the new certificates
			cmds = append(cmds, fmt.Sprintf(`echo "%s" > %s%s`, certData, pair.Remote, cert.Name()))
			// Set file permission
			cmds = append(cmds, fmt.Sprintf("chmod %04o %s", pair.Permission, pair.Remote+cert.Name()))
		}

		log.Printf("| 🟨  Deploying certificates for the host :%v...\n", pair.Host)
		idxSshConf, err := scp.GetConfigIndexByHost(sshConfigs, pair.Host)
		if err == scp.ErrHostNotFound {
			log.Fatalf("| 🟥  No SSH configuration found for the host : %v\n", pair.Host)
		}

		// connect to SSH
		log.Printf("| 🟨  Trying to connect to the host :%v...\n", pair.Host)
		if sshConfigs[idxSshConf].IdentityFile != "" && choiceHostSsh == "y" {
			if err := internalSshCall(sshConfigs[idxSshConf], pair, cmds...); err != nil {
				return err
			}
		} else {
			if sshConfigs[idxSshConf].IdentityFile == "" {
				log.Printf("| 🟧  IdentityFile not found for the host : %v\n", pair.Host)
			}
			log.Printf("| 🟧  Using the user's SSH command.\n")
			if err := hostSshCall(sshConfigs[idxSshConf], pair, cmds...); err != nil {
				return err
			}
		}

		log.Printf("| 🟩  Tasks finished for the host : %v\n", pair.Host)
	}

	return nil
}

func internalSshCall(sshConfig scp.SshConfig, pair config.Pair, cmds ...string) error {
	instance, err := ssh.Connect(sshConfig.HostName+":"+sshConfig.Port, sshConfig.User, sshConfig.IdentityFile)
	if err.Error() == ssh.ErrLoginFailed1.Error() {
		log.Printf("| 🟥  Failed to use the program's SSH\n")
		// Might be due to server misconfiguration or 2FA is setup
		// try to use this instead :
		if err := hostSshCall(sshConfig, pair, cmds...); err != nil {
			return err
		}
	} else if err != nil {
		// Failed to Connect() at all
		log.Fatalf("| 🟥  Failed to connect to the host : %v with error : %v\n", pair.Host, err)
	} else {
		// Successful ssh.Connect()
		log.Printf("| 🟩  Connected successfuly!")

		log.Printf("| 🟨  Deploying %s to host : %v\n", pair.Local, pair.Host)
		if err := instance.RunCommands(cmds...); err != nil {
			log.Fatalf("| 🟥  Failed to run commands on the host : %v, with error : %v\n", pair.Host, err)
		}
	}
	return nil
}

func hostSshCall(sshConfig scp.SshConfig, pair config.Pair, cmds ...string) error {
	log.Printf("| 🟦  Trying to use the user's SSH command...\n")
	err := ssh.LocalSshRunCommands(sshConfig.HostName, sshConfig.Port, sshConfig.User, cmds...)
	if err != nil {
		// Still fails to run commands, even with local SSH command
		log.Fatalf("| 🟥  Unable to run the commands on the host : %v with error : %v\n", pair.Host, err)
	}
	return nil
}

func getCertificateData(path string) (string, error) {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return "", err
	}

	return string(data), nil
}

func getSshParams(confFilePath string) []scp.SshConfig {
	sshConfData, err := ioutil.ReadFile(confFilePath)
	if err != nil {
		log.Fatalf("| 🟥  Couldn't not read %s; err :%v\n", confFilePath, err)
	}
	// Process the Raw data
	rawData := scp.NewRawData(sshConfData)
	// Into segments
	segs, err := rawData.GetSegments()
	if err != nil {
		log.Fatalf("| 🟥  Couldn't not parse segments from %s; \n\terr :%v\n", confFilePath, err)
	}

	// Parsing
	sshConfs, err := segs.Parse()
	if err != nil {
		log.Fatalf("| 🟥  Couldn't not parse configs from %s; \n\terr :%v\n", confFilePath, err)
	}

	hdir, err := os.UserHomeDir()
	if err != nil {
		log.Fatalf("| 🟥  Couldn't find a home directory : %v", err)
	}
	// Fix identity file's path on windows
	if runtime.GOOS == "windows" {
		for i, sc := range sshConfs {
			if sc.IdentityFile != "" {
				sshConfs[i].IdentityFile = strings.ReplaceAll(sc.IdentityFile, "~", hdir)
				sshConfs[i].IdentityFile = filepath.FromSlash(sshConfs[i].IdentityFile)
			}
		}
	}

	return sshConfs
}
