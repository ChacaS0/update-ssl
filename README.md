# update-ssl (CLI)

Takes a bunch of files certificates (key, cert, Intermediate) and put them on the given server.  
Same names will be re-used for the files, old ones will be saved.

## Requirements
* SSL conection to the server is already configured and working
* SSL account used has enough rights
