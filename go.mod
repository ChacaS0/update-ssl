module gitlab.com/ChacaS0/update-ssl

go 1.13

require (
	github.com/leaanthony/clir v1.0.4
	gitlab.com/ChacaS0/ssh_config_parser v1.1.1
	golang.org/x/crypto v0.0.0-20210322153248-0c34fe9e7dc2
	golang.org/x/term v0.0.0-20210317153231-de623e64d2a6
	gopkg.in/yaml.v2 v2.4.0
)
