package config

import (
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"

	"gopkg.in/yaml.v2"
)

// ConfStructure represents how the configuration file
// should be written/built
type ConfStructure struct {
	Certs        []Pair
	SshConfFile  string `yaml:"sshConfFile"`
	KeepOldCerts bool   `yaml:"keepOldCerts"`
}

type Pair struct {
	Host       string      `yaml:"host"`
	Local      string      `yaml:"local"`
	Remote     string      `yaml:"remote"`
	Permission os.FileMode `yaml:"permission"`
}

// New creates a new ConfStructure and returns it
func New() ConfStructure {
	return ConfStructure{}
}

// NewAndLoad creates a new config and loads data into it
func NewAndLoad(configPath string) *ConfStructure {
	cfs := ConfStructure{}
	if err := cfs.LoadConfig(configPath); err != nil {
		log.Fatalln("| ❌  err:", err)
	}

	if cfs.SshConfFile == "" {
		// set default ssh conf file path as ~/.ssh/config
		hdir, err := os.UserHomeDir()
		if err != nil {
			log.Fatalln("| ❌  Couldn't not use user's home directory to find the SSH config file :", err)
		}
		cfs.SshConfFile = hdir + string(os.PathSeparator) + ".ssh/config"
	}
	return &cfs
}

// LoadConfig takes the config file and lods it on the struct
func (cfs *ConfStructure) LoadConfig(configPath string) (err error) {
	if data, err := ioutil.ReadFile(configPath); err == nil {
		err = yaml.Unmarshal(data, &cfs)
		if err != nil {
			return err
		}
	} else {
		// Config file doesn't exist, create then
		createDefaultConf(configPath)
	}
	return err
}

// createDefaultConf Checks if config file exists
// if doesn't exist, create a default one
func createDefaultConf(configPath string) {
	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		log.Fatal("| ❌  ", err)
	}
	example, err := ioutil.ReadFile(dir + string(os.PathSeparator) + "config.example.yml")
	if err != nil {
		log.Fatalln("| ❌  ", err)
		return
	}
	// write the new config file
	err = ioutil.WriteFile(configPath, example, 0644)
	if err != nil {
		// if cannot write, maybe parent directories are missing
		// so create them
		if err = os.MkdirAll(strings.TrimSuffix(configPath, "config.yml"), os.ModePerm); err != nil {
			log.Fatal("| ❌ Could not create parent directory for the configuration file with the error :", err)
		}
		// try again to write the default config file
		err = ioutil.WriteFile(configPath, example, 0644)
		if err != nil {
			log.Println("| ❌ Error creating", configPath)
			log.Fatalln("| ❌ ", err)
		}
	}
	log.Println("| ➕  Created a default/example", configPath)
	log.Println("| 🔧  Edit this file to your convenience and run the program again.")
}
